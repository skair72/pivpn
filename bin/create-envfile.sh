#!/bin/bash

if [[ ! -f `pwd`'/.env' ]]; then

    function get_random {
        echo `openssl rand -base64 500 | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`
    }

    function get_random_port {
        while true
        do
            random_port=$(( ((RANDOM<<15)|RANDOM) % 49152 + 10000 ))
            status="$(nc -z 127.0.0.1 $random_port < /dev/null &>/dev/null; echo $?)"
            if [ "${status}" != "0" ]; then
                echo "$random_port";
                exit;
            fi
        done
    }

    touch `pwd`'/.env'
    echo "PROXY_USER=$(get_random)
PROXY_PASSWORD=$(get_random)
PROXY_PORT=$(get_random_port)" > `pwd`'/.env'
echo 'Done.'
else
echo 'Exists.'
fi

