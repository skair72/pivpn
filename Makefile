default: run

set-container:
	$(eval $(call set-default-container))

env:
	./bin/create-envfile.sh

setup-ovpn:
	docker-compose run --rm ovpn ovpn_genconfig -u udp://$(shell curl -s -4 icanhazip.com)
	docker-compose run --rm ovpn ovpn_initpki
	
create-client:
	docker-compose run --rm ovpn easyrsa build-client-full client nopass
	docker-compose run --rm ovpn ovpn_getclient client > client.ovpn


setup-pihole:
	sudo systemctl disable systemd-resolved.service
	sudo systemctl stop systemd-resolved.service

pihole-password:
	docker-compose logs pihole | grep random

build: set-container
	docker-compose build ${c}

run:
	docker-compose up -d --force-recreate ${c}

restart: set-container
	docker-compose restart ${c}

stop: set-container
	docker-compose stop ${c}

down:
	docker-compose down

exec: set-container
	docker-compose exec ${c} /bin/bash

log: set-container
	docker-compose logs -f ${c}
